/**
 * @file
 * The code below will provide some og_user_identity selection handling.
 */

(function ($) {
  Drupal.behaviors.ogUserGroupSelectionHandling = {
    attach: function (context, settings) {

      // Selectbox autosubmition.
      $('#og-user-identity-group-block-content-form .form-submit').hide();
      $("#edit-user-group-list").change(function () {
        $('#og-user-identity-group-block-content-form').submit();
      });

    }
  };

})(jQuery);

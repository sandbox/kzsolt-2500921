Og User Identity Module
------------------------------
by Keresztes Mozes Zsolt, keresztesmozes@yahoo.com

Description
-----------
  This module implements additional functionality for OG module, extends 
og module, provide some UI and backend feature. Necessary to use together with
og module.
  Very helpful on drupal sites where has been used og module and users have to
switch between groups. Ex.: User has 4 membership and have to add some content
to first/second and so on. There is one feature which populate currently
selected group id into og_group_ref filed.
  In module has been implemented one admin interface, where can found some
feature settings. Also has been implemented on drupal block which contain all
group where user has member.(This list is configurable by role and so on).
  Module use one session variable('og_user_identity') to store selected group 
entity type, bundle and entity id.

Installation 
------------
 + Copy the module's directory to your modules directory.
 + Activate the module.
 + Go to admin interface(admin/config/group/identity) set entity type, bundle,
 select box behaviors and so on ...

Dependencies
------------
Rules
OG

Features
--------
+ debug mode - render session variable content as drupal message.
+ auto selection - auto populate selected group to 'og group audience'
(og_group_ref) reference filed if it has been used in some entity.
+ use "myself" - user can brows on site as on behalf of group or in her/his name
+ get selected group parameter(entity_id, entity_type, entity_bundle) as
drupal token or in rules as rules variable.
+ configurable redirect urls(can use tokens).
+ configurable select box

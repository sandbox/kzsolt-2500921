<?php

/**
 * @file
 * OG user identity Rules related functionality.
 */

/**
 * Implements hook_rules_condition_info().
 */
function og_user_identity_rules_condition_info() {

  $conditions = array();
  $conditions['og_user_identity_rules_is_identity_of'] = array(
    'label' => t('Check OG user identity'),
    'group' => t('Organic groups'),
    // Parameters are described identically to how they work for actions.
    'parameter' => array(
      'identity_type' => array(
        'type' => 'text',
        'label' => t('Select user identity to check'),
        'options list' => 'og_user_identity_rules_identity_type',
      ),
    ),
  );

  $conditions['og_user_identity_rules_entity_is_identity'] = array(
    'label' => t('Check if entity is the OG user identity'),
    'group' => t('Organic groups'),
    // Parameters are described identically to how they work for actions.
    'parameter' => array(
      'target' => array(
        'type' => 'entity',
        'label' => t('Select an entity to check'),
      ),
    ),
  );

  $conditions['og_user_identity_user_use_path'] = array(
    'label' => t('User has been used sample path'),
    'group' => t('Path'),
    // Parameters are define path sample.
    'parameter' => array(
      'path_sample' => array(
        'type' => 'text',
        'label' => t('Path sample. Example: node/%/edit'),
        'description' => t('Example: node/%/edit'),
      ),
    ),
  );

  return $conditions;
}

/**
 * Implements hook_rules_action_info().
 */
function og_user_identity_rules_action_info() {
  $action = array();
  $action['og_user_identity_rules_entity_id'] = array(
    'label' => t('Return identity id'),
    // Parameters are described identically to how they work for actions.
    'parameter' => array(
      'identity_type' => array(
        'type' => 'text',
        'label' => t('Select user identity to return id'),
        'options list' => 'og_user_identity_rules_identity_type',
      ),
    ),
    'group' => t('Organic groups'),
    'provides' => array(
      'identity_id' => array('type' => 'integer', 'label' => t('Identity id')),
    ),
    'base' => 'og_user_identity_variable_add',
  );

  $action['og_user_identity_fetch_selected_group'] = array(
    'label' => t('Fetch selected group from session'),
    'group' => t('Organic groups'),
    'parameter' => array(
      'field_id' => array(
        'type' => 'text',
        'label' => t('Group complete boolean field id'),
        'options list' => 'og_user_identity_fetch_selected_group_action_options',
        'description' => t('Specifies field id of entity that should be fetched. Note: You can select just field that type is boolean.'),
        'restriction' => 'input',
      ),
    ),
    'provides' => array(
      'selected_group' => array('type' => 'unknown', 'label' => t('Selected group')),
      'selected_group_complete' => array('type' => 'integer', 'label' => t('Selected group complete')),
      'selected_group_uuid' => array('type' => 'integer', 'label' => t('Selected group uuid')),
    ),
  );

  $action['og_user_identity_selected_group_info'] = array(
    'label' => t('Get selected group info'),
    'group' => t('Organic groups'),
    'parameter' => array(),
    'provides' => array(
      'entity_type' => array('type' => 'text', 'label' => t('Group Entity Type')),
      'entity_bundle' => array('type' => 'text', 'label' => t('Group Entity Bundle')),
      'entity_id' => array('type' => 'integer', 'label' => t('Group Entity ID')),
    ),
  );

  return $action;
}

/**
 * Return selected groups.
 */
function og_user_identity_fetch_selected_group($field_id) {
  $sel_group = og_user_identity_selected_entity_type_bundle_eid_from_session();
  $entity_type = $sel_group['entity_type'];
  $bundle = $sel_group['bundle'];
  $eid = $sel_group['eid'];

  if ($entity_type != 'user' && strlen($bundle) && $eid) {
    $entity = entity_load_single($entity_type, $eid);

    if (property_exists($entity, $field_id)) {
      $selected_group_complete = isset($entity->{$field_id}[LANGUAGE_NONE][0]['value']) ? $entity->{$field_id}[LANGUAGE_NONE][0]['value'] : -1;
    }
    else {
      $selected_group_complete = -1;
    }

    return array(
      'selected_group' => $entity,
      'selected_group_complete' => $selected_group_complete,
      'selected_group_uuid' => isset($entity->field_company_uuid[LANGUAGE_NONE][0]['value']) ? $entity->field_company_uuid[LANGUAGE_NONE][0]['value'] : '',
    );
  }
  else {
    // Return 0 if selected group not found or if has been selected "MySelf"
    return array('selected_group' => 0);
  }
}

/**
 * Return selected group info.
 */
function og_user_identity_selected_group_info() {

  // Get Selected group info.
  $sel_group_info = _og_user_identity_get_selecteg_group_info();

  return array(
    'entity_type' => $sel_group_info['entity_type'],
    'entity_bundle' => $sel_group_info['entity_bundle'],
    'entity_id' => $sel_group_info['entity_id'],
  );
}

/**
 * Return selected group entity_type, bundle and eid.
 */
function og_user_identity_selected_entity_type_bundle_eid_from_session() {
  $entity_type = '';
  $bundle = '';
  $eid = 0;
  if (isset($_SESSION['og_user_identity'])) {
    // Get selected entity type from session.
    $entity_type = '';
    foreach ($_SESSION['og_user_identity'] as $key => $value) {
      $entity_type = $key;
      break;
    }
    if (strlen($entity_type) && $entity_type != 'user' && isset($_SESSION['og_user_identity'][$entity_type])) {
      // Get selected bundle from session.
      $bundle = '';
      $eid = 0;
      foreach ($_SESSION['og_user_identity'][$entity_type] as $key => $value) {
        $bundle = $key;
        $eid = $value;
        break;
      }
    }
    if ($entity_type == 'user') {
      $eid = (int) $_SESSION['og_user_identity']['user'];
    }
  }

  return array(
    'entity_type' => $entity_type,
    'bundle' => !empty($bundle) ? $bundle : '',
    'eid' => $eid ? $eid : 0,
  );
}

/**
 * Return field array of fields which have boolean type.
 */
function og_user_identity_fetch_selected_group_action_options() {
  $options = array();
  foreach (field_info_fields() as $field_name => $field) {
    if ($field['type'] == 'list_boolean') {
      $options[$field_name] = $field_name;
    }
  }
  return $options;
}

/**
 * The callback for the condition 'og_user_identity_user_use_path'.
 */
function og_user_identity_user_use_path($path_sample) {
  $on_path = TRUE;
  // Build path arrays.
  $path_sample = explode('/', trim(strtolower($path_sample)));
  $path_real = explode('/', strtolower(current_path()));

  // Compare path arrays.
  if (count($path_sample) == count($path_real) && count($path_sample) && count($path_real)) {
    foreach ($path_sample as $key => $path_item) {
      if ($path_item != '%' && $path_sample[$key] != $path_real[$key]) {
        $on_path = FALSE;
      }
    }
  }
  else {
    $on_path = FALSE;
  }

  return $on_path;
}

/**
 * Get id of the selected identity.
 *
 * @return array
 *   Return variable to rules containing the selected identity id.
 */
function og_user_identity_variable_add($args, $element) {
  $identity = explode('-', $args);
  $identity_id = og_user_identity_api_is_group($identity[0], $identity[1]);
  return array('identity_id' => $identity_id);
}

/**
 * Helper function to build a select list over all relation types on the site.
 */
function og_user_identity_rules_identity_type() {
  $groups = og_get_all_group_bundle();
  $options = array(
    'user-' => t('User'),
  );

  foreach ($groups as $entity_type => $bundle) {
    foreach ($bundle as $id => $name) {
      $bundle[$entity_type . '-' . $id] = $name;
      unset($bundle[$id]);
    }
    $options[$entity_type] = $bundle;
  }
  return $options;
}

/**
 * Callback for 'relation_extend_condition_entity_entity_relation_exists'.
 */
function og_user_identity_rules_is_identity_of($identity) {

  $identity = explode('-', $identity);
  return og_user_identity_api_is_group($identity[0], $identity[1]);
}

/**
 * The callback for the condition 'og_user_identity_rules_entity_is_identity'.
 */
function og_user_identity_rules_entity_is_identity($entity) {

  $identity_id = og_user_identity_api_identity_is_of($entity->type(), $entity->entityKey('bundle') ? $entity->value()->{$entity->entityKey('bundle')} : '');

  if ($identity_id) {
    $entity_id = $entity->value()->{$entity->entityKey('id')};
    if ($entity_id == $identity_id) {
      return TRUE;
    }
  }

  return FALSE;
}

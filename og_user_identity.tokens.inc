<?php

/**
 * @file
 * OG user identity Token related functionality.
 */

/**
 * Implements hook_token_info().
 */
function og_user_identity_token_info() {

  $info = array();

  // Define a new TOKEN TYPE.
  $info['types']['selected_entity'] = array(
    'name' => t('Selected entity'),
    'description' => t('Tokens related to Selected entity.'),
  );

  // Define any new TOKEN.
  $info['tokens']['selected_entity']['id'] = array(
    'name' => t('Selected entity id'),
    'description' => t('Selected entity id.'),
  );

  $info['tokens']['selected_entity']['title'] = array(
    'name' => t('Selected entity title'),
    'description' => t('Selected group title.'),
  );

  $info['tokens']['selected_entity']['current_user_id'] = array(
    'name' => t('Current user id'),
    'description' => t('Return current user ID.'),
  );

  return $info;
}

/**
 * Implements hook_tokens().
 */
function og_user_identity_tokens($type, $tokens, array $data = array(), array $options = array()) {

  $replacements = array();

  if ($type == 'selected_entity') {
    // Loop through the available tokens.
    foreach ($tokens as $name => $original) {
      // Find our custom tokens by name.
      switch ($name) {
        case 'id':
          // Work out the value of our token.
          $value = _og_user_identity_get_ec_company_group();
          // Give our token it's value!
          $replacements[$original] = $value;
          break;

        case 'title':
          // Work out the value of our token.
          $group_id = _og_user_identity_get_ec_company_group();
          // Rewrite.
          $value = $group_id ? entity_load_single(variable_get('og_user_identity_entity', ''), $group_id)->title : t("NONE");
          // Give our token it's value!
          $replacements[$original] = $value;
          break;

        case 'current_user_id':
          // Work out the value of our token.
          global $user;
          $value = $user->uid;
          // Give our token it's value!
          $replacements[$original] = $value;
          break;
      }
    }
  }

  return $replacements;
}

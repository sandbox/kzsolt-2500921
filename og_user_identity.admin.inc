<?php

/**
 * @file
 * Provides the OG user identity functionality for client sites.
 */

/**
 * Callback function for og_user_identity setting form.
 */
function og_user_identity_setting_form($form, &$form_state) {
  // Creat form elements.
  $form['og_user_identity_use_debug_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Debug mode.'),
    '#description' => t('Debug mode: show og indentity session contents.'),
    '#default_value' => variable_get('og_user_identity_use_debug_mode', 0),
  );

  $form['og_user_identity_auto_submit'] = array(
    '#type' => 'checkbox',
    '#title' => t('Auto submit.'),
    '#description' => t('Submit automaticaly selection form when selected value has been changed.'),
    '#default_value' => variable_get('og_user_identity_auto_submit', 0),
  );

  $form['og_user_identity_use_myself'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use "MySelf" option.'),
    '#description' => t('Use "MySelf" option in OG User Identity groups list.'),
    '#default_value' => variable_get('og_user_identity_use_myself', 0),
  );

  $form['og_user_identity_use_myself_text'] = array(
    '#type' => 'textfield',
    '#title' => t('Myself text'),
    '#default_value' => variable_get('og_user_identity_use_myself_text', 'Myself'),
    '#states' => array(
      // Hide container when "myself" not used.
      'visible' => array(
        ':input[name="og_user_identity_use_myself"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['og_user_identity_use_og_ref_autopopulate'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use autopopulate on Groups audience field.'),
    '#description' => t('Populate selected group id into Group Audience field.'),
    '#default_value' => variable_get('og_user_identity_use_og_ref_autopopulate', 0),
  );

  $form['og_user_identity_hide_og_group_ref_field'] = array(
    '#type' => 'checkbox',
    '#title' => t('Hide Groups audience field.'),
    '#description' => t('Hide Groups audience field if populate is succesfull.'),
    '#default_value' => variable_get('og_user_identity_hide_og_group_ref_field', 0),
    '#states' => array(
      'visible' => array(
        ':input[name="og_user_identity_use_og_ref_autopopulate"]' => array('checked' => TRUE),
      ),
    ),
  );

  $form['og_user_identity_radio_role'] = array(
    '#type' => 'radios',
    '#title' => t('List groups'),
    '#description' => t('OG User Identity list groups settings'),
    '#options' => array(0 => 'All', 1 => 'By Role'),
    '#default_value' => variable_get('og_user_identity_radio_role', 0),
  );

  $form['og_user_identity_role_container'] = array(
    '#type' => 'container',
    '#title' => 'Roles',
    '#tree' => TRUE,
    '#states' => array(
      // Hide container when "Site" value selected.
      'invisible' => array(
        ':input[name="radio_role"]' => array(
          'value' => 0,
        ),
      ),
    ),
  );

  // Get the entity list from this Drupal and populate an array.
  foreach (entity_get_info() as $entity => $value) {
    $options[$entity] = $value['label'];
  }

  // Saved Entity type form field.
  $form['og_user_identity_entity'] = array(
    '#type' => 'select',
    '#title' => t('Entity type'),
    '#options' => $options,
    '#ajax' => array(
      'callback' => 'og_user_identity_bundle_select_ajax_callback',
      'wrapper' => 'bundle-select',
      'method' => 'replace',
      'effect' => 'fade',
    ),
    '#default_value' => variable_get('og_user_identity_entity', key($options)),
    '#states' => array(
      // Hide container when "Site" value selected.
      'invisible' => array(
        ':input[name="type"]' => array('value' => 0),
      ),
    ),
  );

  // Saved Entity Bundel form field.
  $form['og_user_identity_bundle'] = array(
    '#type' => 'select',
    '#title' => t('Bundle'),
    '#prefix' => '<div id="bundle-select">',
    '#suffix' => '</div>',
    '#validated' => TRUE,
    '#default_value' => variable_get('og_user_identity_bundle', NULL),
    '#states' => array(
      // Hide container when "Site" value selected.
      'invisible' => array(
        ':input[name="type"]' => array('value' => 0),
      ),
    ),
  );

  // Use redirect after form submit config file.
  $form['og_user_identity_use_page_redirect'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use Page redirect.'),
    '#description' => t('Use page redirect after selection form submit.'),
    '#default_value' => variable_get('og_user_identity_use_page_redirect', 0),
  );

  // Myself page redirect.
  $form['og_user_identity_myself_page_redirect_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Myself page redirect'),
    '#description' => t('If myself has been selected then will redirect for this url'),
    '#default_value' => variable_get('og_user_identity_myself_page_redirect_url', ''),
    '#states' => array(
      // Hide container when "myself" not used.
      'visible' => array(
        ':input[name="og_user_identity_use_page_redirect"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Group Redirect.
  $form['og_user_identity_group_page_redirect_url'] = array(
    '#type' => 'textfield',
    '#title' => t('Group page redirect'),
    '#description' => t('If some group has been selected then will redirect for this url'),
    '#default_value' => variable_get('og_user_identity_group_page_redirect_url', ''),
    '#states' => array(
      // Hide container when "myself" not used.
      'visible' => array(
        ':input[name="og_user_identity_use_page_redirect"]' => array('checked' => TRUE),
      ),
    ),
  );

  // Token tree.
  if (module_exists('token')) {

    $form['token_tree'] = array(
      '#type' => 'fieldset',
      '#title' => t('Replacement patterns'),
      '#collapsible' => FALSE,
      '#states' => array(
        // Hide container when "myself" not used.
        'visible' => array(
          ':input[name="og_user_identity_use_page_redirect"]' => array('checked' => TRUE),
        ),
      ),
    );

    $form['token_tree']['patterns'] = array(
      '#theme' => 'token_tree',
      '#token_types' => 'all',
      '#dialog' => TRUE,
    );
  }
  else {
    $form['token_tree'] = array(
      '#markup' => '<p>' . t('Enable the <a href="@drupal-token">Token module</a> to view the available token browser.', array('@drupal-token' => 'http://drupal.org/project/token')) . '</p>',
    );
  }

  // Get the selected entity bundle list and populate an array.
  $info = entity_get_info($form['og_user_identity_entity']['#default_value']);
  foreach ($info['bundles'] as $entity => $value) {
    $opt[$entity] = $value['label'];
  }
  $form['og_user_identity_bundle']['#options'] = $opt;

  // Get all OG group bundle.
  $group_bundles = og_get_all_group_bundle();
  // Get saved roles.
  $saved_roles = variable_get('og_user_identity_role_container', array());
  foreach ($group_bundles as $entity_type => $bundles) {
    foreach ($bundles as $bundle_name => $bundle_label) {
      $og_roles = og_roles($entity_type, $bundle_name, 0, FALSE, FALSE);
      foreach ($og_roles as $role_id => $role_name) {
        if (isset($saved_roles['role_' . $role_id]['enable'])) {
          $role_enable = $saved_roles['role_' . $role_id]['enable'];
        }
        else {
          $role_enable = 0;
        }

        $form['og_user_identity_role_container']['role_' . $role_id]['role_id'] = array(
          '#type' => 'value',
          '#value' => $role_id,
        );

        $form['og_user_identity_role_container']['role_' . $role_id]['enable'] = array(
          '#type' => 'checkbox',
          '#title' => $role_name,
          '#description' => t('Entity: @entity_type, Bundle: @bundle_label', array('@entity_type' => $entity_type, '@bundle_label' => $bundle_label)),
          '#default_value' => $role_enable,
        );
      }
    }
  }

  return system_settings_form($form);
}

/**
 * Callback function for og user identity setting form validate.
 */
function og_user_identity_setting_form_validate($form, &$form_state) {
  if ($form_state['values']['og_user_identity_radio_role']) {
    $has_selected_role = FALSE;
    foreach ($form_state['values']['og_user_identity_role_container'] as $role) {
      if ($role['enable']) {
        $has_selected_role = TRUE;
        break;
      };
    }
    if (!$has_selected_role) {
      form_set_error('og_user_identity_role_container', t('Pleace select role!'));
    }
  }
}

/**
 * Ajax callback function.
 */
function og_user_identity_bundle_select_ajax_callback($form, &$form_state) {
  // Get the selected entity bundle list and populate an array.
  $info = entity_get_info($form_state['values']['og_user_identity_entity']);
  foreach ($info['bundles'] as $entity => $value) {
    $opt[$entity] = $value['label'];
  }
  $form['og_user_identity_bundle']['#options'] = $opt;

  return $form['og_user_identity_bundle'];
}

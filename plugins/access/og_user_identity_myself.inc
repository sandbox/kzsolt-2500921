<?php
/**
 * @file
 * Plugin to provide access control based upon current identity.
 */

/**
 * Pugin definition.
 */
// Plugins are described by creating a $plugin array which will be used by the
// system that includes this file.
$plugin = array(
  'title' => t('OG user identity: Myself'),
  'description' => t("Access based on the user's chosen identity."),
  'callback' => 'og_user_identity_myself_ctools_access_check',
  'default' => array('myself' => 'myself'),
  'settings form' => 'og_user_identity_myself_ctools_access_settings',
  'summary' => 'og_user_identity_myself_ctools_access_summary',
);

/**
 * Settings form for the 'term depth' access plugin.
 */
function og_user_identity_myself_ctools_access_settings($form, &$form_state, $conf) {

  $form['settings']['og_user_identity'] = array(
    '#title' => t('Select OG user identity'),
    '#type' => 'select',
    '#options' => array(
      'myself' => t('myself'),
      'company' => t('a company'),
    ),
    '#description' => t("Access based on the user's chosen identity."),
    '#default_value' => $conf['myself'],
    '#required' => TRUE,
  );

  return $form;
}

/**
 * Check for access.
 */
function og_user_identity_myself_ctools_access_check($conf, $context) {

  if (user_is_logged_in()) {
    if (isset($_SESSION['og_user_identity'])) {
      switch ($conf['og_user_identity']) {

        case 'myself':
          return (isset($_SESSION['og_user_identity']['user']) && !empty($_SESSION['og_user_identity']['user']));

        case 'company':
          return (!isset($_SESSION['og_user_identity']['user']) ||
            (isset($_SESSION['og_user_identity']['user']) && empty($_SESSION['og_user_identity']['user'])));

        default:
          return FALSE;

      }
    }
    else {
      return FALSE;
    }
  }
  else {
    return FALSE;
  }
}

/**
 * Provide a summary description based upon the checked terms.
 */
function og_user_identity_myself_ctools_access_summary($conf, $context) {
  if ($conf['og_user_identity']) {
    return t('Logged in user is acting as "@value"', array('@value' => $conf['og_user_identity']));
  }
}

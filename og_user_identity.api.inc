<?php

/**
 * @file
 * OG user identity API functions.
 */

/**
 * Check if user is user (actions are taken by himself).
 */
function og_user_identity_api_is_user() {
  return og_user_identity_api_identity_is_of('user', '');
}

/**
 * Check if user is group (actions are taken in the name of the OG group).
 */
function og_user_identity_api_is_group($entity_type, $bundle) {

  return og_user_identity_api_identity_is_of($entity_type, $bundle);
}

/**
 * Check if user is ... (actions are taken in the name of a specific OG group).
 */
function og_user_identity_api_identity_is_of($entity_type, $bundle) {

  if (isset($_SESSION['og_user_identity'][$entity_type][$bundle])) {
    return $_SESSION['og_user_identity'][$entity_type][$bundle];
  }
  return FALSE;
}

/**
 * Sett the user identity in session (User will act in name of entity set).
 *
 * @param string $identity_id
 *   The id of the entity the user acts for.
 */
function og_user_identity_api_set_user_identity($identity_id, $entity_type, $bundle) {
  unset($_SESSION['og_user_identity']);
  $_SESSION['og_user_identity'][$entity_type][$bundle] = $identity_id;
}

/**
 * Form select - update option with OG groups for current user.
 */
function og_user_identity_api_element_options(&$element, $og_role_ids = array(), $og_group_type = NULL) {
  global $user;
  if (variable_get('og_user_identity_use_myself', 0)) {
    $element['#options'] = array('user-' . $user->uid => variable_get('og_user_identity_use_myself_text', 'Myself'));
  }
  $element['#default_value'] = isset($_SESSION['og_user_identity']) ? $_SESSION['og_user_identity'] : '';
  if (isset($_SESSION['og_user_identity']) && !empty($_SESSION['og_user_identity'])) {
    $value_type = array_keys($_SESSION['og_user_identity']);
    $entity_type = $value_type[0];
    $value_id = array_values($_SESSION['og_user_identity']);
    $entity_id = array_values($value_id[0]);
    $element['#default_value'] = $entity_type . '-' . $entity_id[0];
  }
  // Get groups.
  $groups = og_user_identity_get_all_groups($user, $og_role_ids, $og_group_type);

  if (is_array($groups)) {
    foreach ($groups as $entity_type => $value) {
      foreach ($value as $entity_id => $value) {
        $entity = og_user_identity_api_get_entity_metadata_wrapper($entity_type, $entity_id);
        if ($entity) {
          $bundle = $entity->value()->{$entity->entityKey('bundle')};
          $info = entity_get_info($entity_type);
          $label = !empty($info['bundles'][$bundle]['label']) ? $info['bundles'][$bundle]['label'] : $bundle;
          $element['#options'][$label][$entity_type . '-' . $entity->value()->{$entity->entityKey('id')}] = $entity->value()->{$entity->entityKey('label')};
        }
      }
    }
  }

  // Allow other modules to alter element options.
  drupal_alter('og_user_identity_options', $element);
}

/**
 * Load enity and return the metadata value.
 */
function og_user_identity_api_get_entity_metadata_wrapper($entity_type, $entity_id) {
  $entity = entity_load_single($entity_type, $entity_id);
  if ($entity) {
    return entity_metadata_wrapper($entity_type, $entity);
  }
  return NULL;
}

/**
 * Load entity and return the metadata value.
 *
 * Load user group where user is admin member and return array of groups.
 */
function og_user_identity_get_groups_by_user_is_admin_of_group($uid, $groups, $og_role_ids, $og_group_type = NULL) {
  if (!$og_group_type) {
    $og_group_type = variable_get('og_user_identity_entity', NULL);
  }
  if (!isset($groups[$og_group_type])) {
    return $groups;
  }
  // Limited group just $og_group_type.
  $groups2[$og_group_type] = $groups[$og_group_type];
  $groups = $groups2;
  // Get group id where user is admin member.
  $query = db_select('og_users_roles', 'og');
  $query->condition('gid', $groups[$og_group_type], 'IN');
  $query->condition('uid', $uid, '=');
  $query->condition('rid', $og_role_ids, 'IN');
  $query->condition('group_type', $og_group_type, '=');
  $query->fields('og', array('gid'));
  $result = $query->execute()->fetchAllAssoc('gid');
  $result = array_keys($result);
  // Build $groups array structure.
  $result2 = array();
  foreach ($result as $value) {
    $result2[$value] = "$value";
  }
  $result = $result2;
  $groups[$og_group_type] = $result;

  return $groups;
}

/**
 * Return All groups.
 */
function og_user_identity_get_all_groups($user, $og_role_ids, $og_group_type = NULL) {
  if (!$og_group_type) {
    $og_group_type = variable_get('og_user_identity_entity', NULL);
  }
  $groups = og_get_groups_by_user($user);
  if (count($og_role_ids)) {
    // @TODO If slow then implement feature with one query with one JOIN.
    $groups = og_user_identity_get_groups_by_user_is_admin_of_group($user->uid, $groups, $og_role_ids, $og_group_type);
  }
  return $groups;
}

/**
 * Get selected groups entity type.
 */
function _og_user_identity_get_selecteg_group_info() {

  $entity_type = '';
  $entity_bundle = '';
  $selected_entity_id = -1;

  if (isset($_SESSION['og_user_identity'])) {
    foreach ($_SESSION['og_user_identity'] as $e_type => $t_value) {

      $entity_type = $e_type;
      foreach ($t_value as $bundle => $id) {

        $entity_bundle = $bundle;
        $selected_entity_id = $id;
        break;
      }
      break;
    }
  }

  if ($entity_type != '' && $entity_bundle != '' && $selected_entity_id != -1) {
    return array(
      'entity_type' => $entity_type,
      'entity_bundle' => $entity_bundle,
      'entity_id' => $selected_entity_id,
    );
  }
  else {
    FALSE;
  }
}

/**
 * Get the value of [advertiser:group] token.
 */
function _og_user_identity_get_ec_company_group() {

  $entity_type = variable_get('og_user_identity_entity', '');
  $bundle = variable_get('og_user_identity_bundle', '');

  if ($entity_type != '' && $bundle != '') {
    // Return selected group id.
    return isset($_SESSION['og_user_identity'][$entity_type][$bundle]) ? $_SESSION['og_user_identity'][$entity_type][$bundle] : FALSE;
  }
  else {
    return FALSE;
  }
}

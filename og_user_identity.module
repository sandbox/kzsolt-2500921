<?php

/**
 * @file
 * OG user identity API functions.
 */

module_load_include('inc', 'og_user_identity', 'og_user_identity.api');
module_load_include('inc', 'og_user_identity', 'og_user_identity.tokens');

/**
 * Implements hook_menu().
 */
function og_user_identity_menu() {

  $items['admin/config/group/identity'] = array(
    'title' => 'OG User identity settings',
    'description' => 'Configure OG user identity.',
    'page callback' => 'drupal_get_form',
    'page arguments' => array('og_user_identity_setting_form'),
    'access arguments' => array('access administer og user identity'),
    'file' => 'og_user_identity.admin.inc',
  );

  return $items;
}

/**
 * Implements hook_permission().
 */
function og_user_identity_permission() {

  $items = array();

  $items['access administer og user identity'] = array(
    'title' => t('Administer og user identity settings form.'),
    'description' => t('Allows users to administer og user identity settings form.'),
  );

  return $items;
}

/**
 * Implements hook_user_login().
 */
function og_user_identity_user_login(&$edit, $user) {

  if (variable_get('og_user_identity_use_myself', 0)) {

    $_SESSION['og_user_identity'] = array(
      'user' => array('' => $user->uid),
    );
  }
  else {

    $entity_type = variable_get('og_user_identity_entity', '');
    $bundle = variable_get('og_user_identity_bundle', '');

    if ($entity_type != '' && $bundle != '') {

      $og_role_ids = _og_user_identity_get_selected_roles();
      $groups = og_user_identity_get_all_groups($user, $og_role_ids, $entity_type);

      $groups = $groups ? reset($groups) : $groups;

      if (count($groups)) {
        $eid = reset($groups);

        // Set Session.
        $_SESSION['og_user_identity'][$entity_type][$bundle] = $eid;
      }
      else {
        $_SESSION['og_user_identity'] = array(
          'user' => array('' => $user->uid),
        );
      }
    }
  }
}

/**
 * Implements hook_init().
 */
function og_user_identity_init() {

  if (variable_get('og_user_identity_use_debug_mode', 0)) {

    if (isset($_SESSION['og_user_identity'])) {

      drupal_set_message(print_r($_SESSION['og_user_identity'], TRUE));
    }
  }
}

/**
 * Implements hook_ctools_plugin_directory().
 */
function og_user_identity_ctools_plugin_directory($owner, $plugin_type) {
  if ($owner == 'ctools' && $plugin_type == 'access') {
    return 'plugins/' . $plugin_type;
  }
}

/**
 * Implements hook_block_info().
 */
function og_user_identity_block_info() {

  $blocks['og_user_identity_group'] = array(
    'info' => t('User identity'),
  );
  return $blocks;
}

/**
 * Implements hook_block_view().
 */
function og_user_identity_block_view($delta = '') {

  switch ($delta) {
    case 'og_user_identity_group':
      $block['subject'] = t('User identity');
      $block['content'] = drupal_get_form('og_user_identity_group_block_content_form');
      break;
  }
  return $block;
}

/**
 * Callback function after  drupal_get_form().
 */
function og_user_identity_group_block_content_form($form, $form_state) {
  global $user;
  $og_group_type = variable_get('og_user_identity_entity', NULL);

  if (user_is_anonymous() || !og_get_groups_by_user($user, $og_group_type)) {
    return;
  }

  $form['user_group_list'] = array(
    '#type' => 'select',
    '#title' => t('Select identity'),
  );
  // Decide group list mode.
  if (!variable_get('og_user_identity_radio_role')) {
    // If selected ALL.
    og_user_identity_api_element_options($form['user_group_list']);
    if (!isset($form['user_group_list']['#options'])) {
      return;
    }
  }
  else {
    // If selected BY ROLE.
    $selected_role_ids = _og_user_identity_get_selected_roles();
    og_user_identity_api_element_options($form['user_group_list'], $selected_role_ids, $og_group_type);
    if (!isset($form['user_group_list']['#options'])) {
      return;
    }
  }

  // Put first itme of user group list into session.
  // Case when user company request has been approved( and
  // not used "MySelf" option).
  if (variable_get('og_user_identity_use_myself', 0) == 0) {

    $sel_group_ingo = _og_user_identity_get_selecteg_group_info();

    if (isset($form['user_group_list']['#options']) && $sel_group_ingo == FALSE && count($form['user_group_list']['#options']) > 0) {

      $options = reset($form['user_group_list']['#options']);

      foreach ($options as $group_key => $value) {

        $group_key_arr = explode('-', $group_key);

        $entity_type = isset($group_key_arr[0]) ? $group_key_arr[0] : '';
        $entity_id = isset($group_key_arr[1]) ? $group_key_arr[1] : '';
        $entity_bundle = variable_get('og_user_identity_bundle', '');
        if ($entity_type != '' && $entity_id != '' && $entity_bundle != '') {
          // Set first group from list.
          og_user_identity_api_set_user_identity($entity_id, $entity_type, $entity_bundle);
        }
        break;
      }
    }
  }

  $form['submit'] = array('#type' => 'submit', '#value' => t('Submit'));

  if (variable_get('og_user_identity_auto_submit', 0) == 1) {

    $form['submit']['#attributes']['style'][] = 'display:none';
    $form['#attached']['js'][] = drupal_get_path('module', 'og_user_identity') . '/og_user_identity.js';
  }

  return $form;
}

/**
 * Validation for identity form dropdown.
 */
function og_user_identity_group_block_content_form_validate($form, $form_state) {

  $info = explode('-', $form_state['values']['user_group_list']);
  $entity = og_user_identity_api_get_entity_metadata_wrapper($info[0], $info[1]);
  // Setup User identity Session with api.
  og_user_identity_api_set_user_identity($entity->value()->{$entity->entityKey('id')}, $info[0], $entity->entityKey('bundle') ? $entity->value()->{$entity->entityKey('bundle')} : '');
}

/**
 * Impements og_user_identity_group_block_content_form form submit.
 */
function og_user_identity_group_block_content_form_submit($form, $form_state) {

  if (variable_get('og_user_identity_use_page_redirect', 0)) {
    // Page redirect.
    global $base_url;

    // Evaluate page redirect.
    $redirect_url = $base_url . '/' . og_user_identity_evaluate_redirect_urls();

    $data = array(
      'form' => $form,
      'form_state' => $form_state,
      'redirect_url' => $redirect_url,
    );

    // Allow other modules to alter the redirect URL.
    drupal_alter('og_user_identity_redirect', $data);

    if ($data['redirect_url'] && url_is_external($data['redirect_url'])) {

      drupal_goto($data['redirect_url']);
    }
  }
}

/**
 * Implements hook_form_alter().
 */
function og_user_identity_form_alter(&$form, $form_state, $form_id) {
  // Populate only when enabled.
  if (!variable_get('og_user_identity_use_og_ref_autopopulate', 0)) {
    return;
  }
  // Validate that we are on an entity add form.
  if (!isset($form['#entity_type']) || !isset($form['#bundle'])) {
    return;
  }
  // Get entity type and bundle from the form.
  $entity_type = $form['#entity_type'];
  $bundle = $form['#bundle'];
  // Make sure we have a valid user identity.
  $sel_group_info = _og_user_identity_get_selecteg_group_info();
  if (!$sel_group_info) {
    return;
  }
  // Loop trough group audience fields, if any.
  foreach (og_get_group_audience_fields($entity_type, $bundle) as $field_name => $label) {
    // Get the field info.
    $field_info = field_info_field($field_name);
    // Compare selected entity to og_group_ref field target.
    if ($field_info['settings']['target_type'] == $sel_group_info['entity_type']) {
      // Populate selected id into group audience field.
      $form[$field_name][LANGUAGE_NONE][0]['default']['#default_value'] = $sel_group_info['entity_id'] ? array($sel_group_info['entity_id']) : array();
      // Hide OG Group Audience field if it has been setuped.
      if (variable_get('og_user_identity_hide_og_group_ref_field', 0)) {
        $form[$field_name]['#attributes']['class'][] = 'element-hidden';
        $form[$field_name]['#attributes']['style'][] = 'display:none';
      }
    }
  }
}

/**
 * Return selected OG role ids.
 */
function _og_user_identity_get_selected_roles() {

  $saved_roles = variable_get('og_user_identity_role_container', array());
  $selected_role_ids = array();
  foreach ($saved_roles as $role) {
    if (isset($role['enable']) && isset($role['role_id']) && $role['enable']) {
      $selected_role_ids[] = $role['role_id'];
    }
  }
  return $selected_role_ids;
}

/**
 * Evaluate page redirects urls.
 */
function og_user_identity_evaluate_redirect_urls() {

  if (_og_user_identity_get_selecteg_group_info() == FALSE) {

    $url_with_token = variable_get('og_user_identity_myself_page_redirect_url', '');
  }
  else {
    $url_with_token = variable_get('og_user_identity_group_page_redirect_url', '');
  }

  return token_replace($url_with_token);
}
